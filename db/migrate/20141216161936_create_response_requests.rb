class CreateResponseRequests < ActiveRecord::Migration
  def change
    create_table :response_requests do |t|
      t.integer :ideaId

      t.timestamps
    end
  end
end
