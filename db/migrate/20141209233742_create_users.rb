class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.float :ideaAmmo

      t.timestamps
    end
  end
end
