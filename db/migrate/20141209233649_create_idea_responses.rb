class CreateIdeaResponses < ActiveRecord::Migration
  def change
    create_table :idea_responses do |t|
      t.text :description
      t.integer :userId
      t.integer :ideaId

      t.timestamps
    end
  end
end
