class AddInProgressToResponseRequests < ActiveRecord::Migration
  def change
    add_column :response_requests, :in_progress, :boolean, default: false
  end
end
