class CreateIdeaResponseComments < ActiveRecord::Migration
  def change
    create_table :idea_response_comments do |t|
      t.integer :idea_response_id
      t.integer :user_id
      t.text :description

      t.timestamps
    end
  end
end
