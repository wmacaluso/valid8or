angular
.module('valid8or')
.controller('ideaResponseCtrl', ['$scope', '$http', function($scope, $http){

  $scope.ideaResponseId = null;
  $scope.newCommentDescription = '';
  $scope.newCommentTextareaIsVisible = false;
  $scope.createIdeaResponseCommentURL = '/idea_response_comments.json';
  $scope.getIdeaResponseCommentHTML_URL = '/idea_response_comments/'

  $scope.addComment = function(idea_response_comment){
    id = idea_response_comment.id
  $http
  .get($scope.getIdeaResponseCommentHTML_URL + id)
  .success(function(data, status, headers, config){
    $('#ideaResponseCommentsDiv').prepend("<hr style=border-color:lightgrey; border-style:dashed;' />");
    $('#ideaResponseCommentsDiv').prepend(data);
  });
  };

$scope.showNewCommentTextarea = function(){
  $scope.newCommentTextareaIsVisible = true;
}

$scope.createIdeaResponseComment = function(commentDesc){

  var newIdeaResponseComment = {
    idea_response_id: $scope.ideaResponseId,
    description: $scope.newCommentDescription

  };

  $http
    .post($scope.createIdeaResponseCommentURL, newIdeaResponseComment)
    .success(function(data, status, headers, config) {
      $scope.addComment(data);
      $scope.newCommentDescription = ''
      $scope.newCommentTextareaIsVisible = false
    })
  .error(function(data, status, headers, config) {

  });
}
}]);

angular
.module('valid8or')
.controller('newIdeaResponseCtrl', ['$scope', '$http', function($scope, $http){

  $scope.response_request_id = null;
  $scope.response_request = null;
  $scope.updateRequestResponseURL = '/response_requests/'
  $scope.getRequestResponseURL = '/response_requests/'

  $scope.get_response_request = function(){
    $scope.response_request = $http
  .get($scope.getRequestResponseURL + $scope.response_request_id)
  .success(function(data, status, headers, config){
    $scope.response_request = data;
  });
  }

$scope.unlock_response_request = function(){

  $http
    .get($scope.getRequestResponseURL + $scope.response_request_id)
    .success(function(data, status, headers, config){
      //$scope.response_request = data;
      data.in_progress = false;
      $http.put($scope.updateRequestResponseURL + data.id, data);
  });


  $scope.response_request.in_progress = false;
  $http.put($scope.updateRequestResponseURL + $scope.response_request_id);
};

$(window).unload(function(){

  var update_URL = $scope.updateRequestResponseURL + $scope.response_request_id + '.json';

  var response_request = {
    id: $scope.response_request_id,
    in_progress: false
  };

  $.ajax({
    url: update_URL,
    type: 'PUT',
    data: JSON.stringify(response_request),
    contentType: 'application/json',
    dataType: 'application/json',
    async: false
  });

  //
  //
  // $.ajax({
  //   url: $scope.getRequestResponseURL + $scope.response_request_id + '.json',
  //   type: 'GET',
  //   dataType: 'application/json',
  //   async: false
  // })
  // .done(function(msg){
  //   response_request = msg.data;
  //
  // response_request.in_progress = false;
  //
  // $.ajax({
  //   url: update_URL,
  //   type: 'PUT',
  //   data: JSON.stringify(response_request),
  //   contentType: 'application/json',
  //   dataType: 'application/json',
  //   async: false
  // });
  //
  //
  // });
  //
  //
  // response_request.in_progress = false;
  //
  // $.ajax({
  //   url: update_URL,
  //   type: 'PUT',
  //   data: JSON.stringify(response_request),
  //   contentType: 'application/json',
  //   dataType: 'application/json',
  //   async: false
  // });
  //
  // //got this from http://stackoverflow.com/questions/1119289/how-to-show-the-are-you-sure-you-want-to-navigate-away-from-this-page-when-ch
  // //e = e || window.event; 
  // var message = 'Are you sure you want to abandon this feedback?';
  //
  // if(confirm(message)){
  //   alert('1');
  // }
  // else{
  //   alert('2');
  // }
  //
  //
  // if($scope.response_request_id != null){
  //   //$scope.get_response_request();
  //   //$scope.unlock_response_request();
  //   var response_request = {
  //     id:  $scope.response_request_id,
  //     in_progress: false
  //   };
  //
  //   $http.put($scope.updateRequestResponseURL, response_request)
  // }
});

}]);


