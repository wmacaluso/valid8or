angular
.module('valid8or')
.controller('ideasCtrl', ['$scope', '$http', function($scope, $http){
  
}]);

angular
.module('valid8or')
.controller('ideaCtrl', ['$scope', '$http', function($scope, $http){

  $scope.ideaId = null;
  $scope.responseRequestsPending = null;
  $scope.createResponseRequestURL = '/response_requests.json';

  $scope.createResponseRequest = function(){
    var newResponseRequest = {
      ideaId: $scope.ideaId
    };

    $http
    .post($scope.createResponseRequestURL, newResponseRequest)
    .success(function(data, status, headers, config) {
      $scope.responseRequestsPending++;
      $scope.$parent.ideaAmmo--;
    })
    .error(function(data, status, headers, config) {

    });

    $scope.requestsPending = $scope.getRequestsPending();

  }
}]);


