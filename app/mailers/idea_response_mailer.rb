class IdeaResponseMailer < ActionMailer::Base
  default from: "info@valid8or.com"

  def comment_received(idea_response, idea_response_comment)
    user = User.find(idea_response.userId)

    @idea_response = idea_response
    @idea_response_comment = idea_response_comment

    mail(to: user.email, subject: 'Response to your startup feedback')
  end

  def feedback_received(user, idea_response)
    @user = user
    @idea_response = idea_response
    @idea = @idea_response.getIdea()
    @ideaPosterUser = @idea.getUser()
    mail(to: @ideaPosterUser.email, subject: 'Response to your startup idea: ' + @idea.title)
  end
end
