json.array!(@idea_responses) do |idea_response|
  json.extract! idea_response, :id, :description, :userId, :ideaId, :ideaResponseOrdinal
  json.url idea_response_url(idea_response, format: :json)
end
