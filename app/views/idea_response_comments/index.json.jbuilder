json.array!(@idea_response_comments) do |idea_response_comment|
  json.extract! idea_response_comment, :id, :idea_response_id, :user_id, :description
  json.url idea_response_comment_url(idea_response_comment, format: :json)
end
