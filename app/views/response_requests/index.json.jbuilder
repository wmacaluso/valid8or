json.array!(@response_requests) do |response_request|
  json.extract! response_request, :id, :ideaId
  json.url response_request_url(response_request, format: :json)
end
