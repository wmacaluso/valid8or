class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  before_action :omniauth_callback

  def omniauth_callback
    #You need to implement the method below in your model (e.g. app/models/user.rb)
    @user = User.from_omniauth(request.env["omniauth.auth"])

    puts @user.inspect

    if @user.persisted?
      sign_in_and_redirect @user, :event => :authentication #this will throw if @user is not activated
      set_flash_message(:notice, :success, :kind => "Facebook") if is_navigational_format?
    else
      session["devise.facebook_data"] = request.env["omniauth.auth"]

      begin
        sign_in_and_redirect @user
      rescue
        #user = User.where('email = ?', @user.email)
        #provider = user['provider']
        #puts user.inspect

        redirect_to root_url, notice: "Please sign in with your original provider"# + provider
      end

      #redirect_to new_user_registration_url
    end

  end

  def twitter
  end

  def linkedin
  end

  def google_oauth2
  end

  def facebook
  end
end
