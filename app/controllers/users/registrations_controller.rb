class Users::RegistrationsController < Devise::RegistrationsController
  # before_filter :configure_sign_up_params, only: [:create]
  # before_filter :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  # def new
  #   super
  # end

  # POST /resource
  # def create
  #   super
  # end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  # def update
  #   super
  # end

  # DELETE /resource
  def destroy
    super do |resource|

      #delete ideas
      ideas = Idea.where('userId = ?', resource.id)

      #delete requests
      ideas.each do |idea|
        response_requests = ResponseRequest.where('ideaId = ?', idea.id)
        response_requests.each{|response_request| response_request.destroy}
      end

      #delete idea responses
      idea_responses = IdeaResponse.where('userId = ?', resource.id)        
      idea_responses.each{|idea_response| idea_response.destroy}

      #delete comments
      comments = IdeaResponseComment.where('user_id = ?', resource.id)
      comments.each{|comment| comment.destroy}

    end

  end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # protected

  # You can put the params you want to permit in the empty array.
  # def configure_sign_up_params
  #   devise_parameter_sanitizer.for(:sign_up) << :attribute
  # end

  # You can put the params you want to permit in the empty array.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.for(:account_update) << :attribute
  # end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
end
