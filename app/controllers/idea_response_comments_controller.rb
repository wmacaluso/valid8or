class IdeaResponseCommentsController < ApplicationController
  before_action :set_idea_response_comment, only: [:show, :edit, :update, :destroy]
  before_filter :only_allow_user, only: [:show, :edit, :update, :destroy]

  def only_allow_user
    if current_user.id != @idea_response_comment.user_id
      redirect_to idea_response_comments_path
    end
  end

  respond_to :html, :json

  def index
    @idea_response_comments = IdeaResponseComment.all
    respond_with(@idea_response_comments)
  end

  def show
    #respond_with(@idea_response_comment)
    render partial: 'idea_response_comments/idea_response_comment', locals: {idea_response_comment: @idea_response_comment}
  end

  def new
    @idea_response_comment = IdeaResponseComment.new
    respond_with(@idea_response_comment)
  end

  def edit
  end

  def create
    @idea_response_comment = IdeaResponseComment.new(idea_response_comment_params)
    @idea_response_comment.user_id = current_user.id
    @idea_response_comment.save
    respond_with(@idea_response_comment)
  end

  def update
    @idea_response_comment.update(idea_response_comment_params)
    respond_with(@idea_response_comment)
  end

  def destroy
    @idea_response_comment.destroy
    respond_with(@idea_response_comment)
  end

  private
  def set_idea_response_comment
    @idea_response_comment = IdeaResponseComment.find(params[:id])
  end

  def idea_response_comment_params
    params.require(:idea_response_comment).permit(:idea_response_id, :user_id, :description)
  end
end
