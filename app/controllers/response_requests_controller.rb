class ResponseRequestsController < ApplicationController
  before_action :set_response_request, only: [:show, :edit, :update, :destroy]

  # GET /response_requests
  # GET /response_requests.json
  def index
    @response_requests = ResponseRequest.all
  end

  # GET /response_requests/1
  # GET /response_requests/1.json
  def show
  end

  # GET /response_requests/new
  def new
    @response_request = ResponseRequest.new
  end

  # GET /response_requests/1/edit
  def edit
  end

  # POST /response_requests
  # POST /response_requests.json
  def create
    @response_request = ResponseRequest.new(response_request_params)

    #determine current idea ammo for this user
    ideaAmmo = current_user.ideaAmmo? ? current_user.ideaAmmo : 0

    #if user has > 1 idea ammo, spend 1 idea ammo
    if ideaAmmo >= 1
      ideaAmmo -= 1
      current_user.ideaAmmo = ideaAmmo
      current_user.save
    else
      format.json { render json: @response_request.errors, status: :unprocessable_entity }
      return
    end

    respond_to do |format|
      if @response_request.save
        format.html { redirect_to @response_request, notice: 'Response request was successfully created.' }
        format.json { render :show, status: :created, location: @response_request }
      else
        format.html { render :new }
        format.json { render json: @response_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /response_requests/1
  # PATCH/PUT /response_requests/1.json
  def update
    respond_to do |format|
      if @response_request.update(response_request_params)
        format.html { redirect_to @response_request, notice: 'Response request was successfully updated.' }
        format.json { render :show, status: :ok, location: @response_request }
      else
        format.html { render :edit }
        format.json { render json: @response_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /response_requests/1
  # DELETE /response_requests/1.json
  def destroy
    @response_request.destroy
    respond_to do |format|
      format.html { redirect_to response_requests_url, notice: 'Response request was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_response_request
    @response_request = ResponseRequest.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def response_request_params
    params.require(:response_request).permit(:ideaId, :in_progress)
  end
end
