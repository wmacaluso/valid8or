class HomeController < ApplicationController
  layout 'devise'

  def splash
    if user_signed_in?
      redirect_to ideas_url
    end
  end

  def about
  end
end
