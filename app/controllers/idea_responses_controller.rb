class IdeaResponsesController < ApplicationController
  before_action :set_idea_response, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_filter :only_allow_user, only: [:edit, :update, :destroy]

  def only_allow_user
    if current_user.id != @idea_response.userId
      redirect_to idea_responses_path
    end
  end

  # GET /idea_responses
  # GET /idea_responses.json
  def index
    @idea_responses = IdeaResponse.getResponsesByUser(current_user)
  end

  # GET /idea_responses/1
  # GET /idea_responses/1.json
  def show
    @idea = @idea_response.getIdea()
    @idea_response.comments = @idea_response.get_comments()
  end

  # GET /idea_responses/new
  def new
    @idea_response = IdeaResponse.new

    #Get oldest response request
    #ideaId of this response request is @idea_response.ideaId
    #Get idea, and all other responses sorted by time

    #Oldest request will have earliest primary key id
    @response_request = ResponseRequest.getFirstForThisUser(current_user)

    if !@response_request.nil?
      @ideaId = @response_request.ideaId
      @idea_response.ideaId = @ideaId
      @idea = Idea.find(@ideaId)
      @earlierResponses = @idea.getResponses()

      #set to in progress
      @response_request.in_progress = true
      @response_request.save
    else
      #No requests...give them a credit if they don't have one.
      current_user.addComplimentaryIdeaAmmoIfEmpty(current_user)
      redirect_to "/error/needResponseRequests"
    end
  end

  # GET /idea_responses/1/edit
  def edit
    @idea_response.comments = @idea_response.get_comments()
    @idea = @idea_response.getIdea()
  end

  # POST /idea_responses
  # POST /idea_responses.json
  def create
    @idea_response = IdeaResponse.new(idea_response_params)
    @idea_response.userId = current_user.id

    respond_to do |format|
      if @idea_response.save
        #format.html { redirect_to @idea_response, notice: 'Idea response was successfully created.' }
        format.html{redirect_to ideas_path, notice: 'Idea response was successfully created.' }

        format.json { render :show, status: :created, location: @idea_response }

        @idea_response.completeResponse(current_user)
      else
        format.html { render :new }
        format.json { render json: @idea_response.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /idea_responses/1
  # PATCH/PUT /idea_responses/1.json
  def update
    respond_to do |format|
      if @idea_response.update(idea_response_params)
        format.html { redirect_to @idea_response, notice: 'Idea response was successfully updated.' }
        format.json { render :show, status: :ok, location: @idea_response }
      else
        format.html { render :edit }
        format.json { render json: @idea_response.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /idea_responses/1
  # DELETE /idea_responses/1.json
  def destroy

    #destroy comments
    comments = @idea_response.get_comments()
    comments.each{|comment| comment.destroy}

    @idea_response.destroy
    respond_to do |format|
      format.html { redirect_to idea_responses_url, notice: 'Idea response was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_idea_response
    @idea_response = IdeaResponse.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def idea_response_params
    params.require(:idea_response).permit(:description, :userId, :ideaId)
  end
end
