class IdeasController < ApplicationController
  before_action :set_idea, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_filter :only_allow_user, only: [:edit, :update, :destroy]

  def only_allow_user
    if current_user.id != @idea.userId
      redirect_to ideas_path
    end
  end

  # GET /ideas
  # GET /ideas.json
  def index
    @get_all_users = params[:all_users]

    if @get_all_users == '0'
      @ideas = Idea.getIdeasByUser(current_user)
    else
      @ideas = Idea.all.limit(20).order(created_at: :desc)
      render 'ideas/idea_wall'
    end

  end

  # GET /ideas/1
  # GET /ideas/1.json
  def show
    @idea_responses = @idea.getResponses()
  end

  # GET /ideas/new
  def new
    @idea = Idea.new
  end

  # GET /ideas/1/edit
  def edit
    @idea_responses = @idea.getResponses()
  end

  # POST /ideas
  # POST /ideas.json
  def create
    @idea = Idea.new(idea_params)
    @idea.userId = current_user.id

    respond_to do |format|
      if @idea.save
        #format.html { redirect_to @idea, notice: 'Idea was successfully created.' }
        format.html { redirect_to ideas_path, notice: 'Idea was successfully created.' }
        format.json { render :show, status: :created, location: @idea }
        
        @idea.createInitialResponseRequest(current_user)

      else
        format.html { render :new }
        format.json { render json: @idea.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ideas/1
  # PATCH/PUT /ideas/1.json
  def update
    respond_to do |format|
      if @idea.update(idea_params)
        format.html { redirect_to @idea, notice: 'Idea was successfully updated.' }
        format.json { render :show, status: :ok, location: @idea }
      else
        format.html { render :edit }
        format.json { render json: @idea.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ideas/1
  # DELETE /ideas/1.json
  def destroy
    @idea.destroy

    #destroy all idea responses, response requests (with refund), comments
    @idea_responses = IdeaResponse.where('ideaId = ?', @idea.id)
    @response_requests = ResponseRequest.where('ideaId = ?', @idea.id)

    current_user.ideaAmmo += @response_requests.count
    current_user.save

    @idea_responses.each do |idea_response|
      comments = idea_response.get_comments()
      comments.each{|comment| comment.destroy}
      idea_response.destroy
    end

    @response_requests.each{ |response_request| response_request.destroy}

    respond_to do |format|
      format.html { redirect_to ideas_url, notice: 'Idea was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_idea
    @idea = Idea.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def idea_params
    params.require(:idea).permit(:title, :description, :userId)
  end
end
