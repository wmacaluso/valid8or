class Idea < ActiveRecord::Base

  attr_accessor :responseRequestsPending
  attr_accessor :responses

  has_one :user

  def getUser()
    return User.find(userId)
  end

  def self.getIdeasByUser(user)
    ideas = Idea.where "userId = ?", user.id

    for idea in ideas
      idea.responseRequestsPending = ResponseRequest.where("ideaId = ?", idea.id).count()
    end

    return ideas
  end

  def createInitialResponseRequest(user)
    @response_request = ResponseRequest.new
    @response_request.ideaId = id
    @response_request.save

    user.addIdeaAmmo(-1, user)
  end

  def getResponses()
    idea_responses = IdeaResponse.where("ideaId = ?", id).order(created_at: :asc)
    idea_responses.map{|idea_response| idea_response.comments = idea_response.get_comments()}
    return idea_responses
  end
end
