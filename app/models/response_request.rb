class ResponseRequest < ActiveRecord::Base
  def self.getFirstForThisUser(user)

    #Get ideas that this user has already commented on
    ideasUserHasAlreadyCommentedOn = IdeaResponse
    .select('ideaId')
    .where('userId = ?', user.id)
    .distinct

    #Get the first request that this user hasn't him/herself posted
    #and also ensure that they haven't already commented on it
    oldestResponseRequest = ResponseRequest
    .joins('INNER JOIN ideas ON response_requests.ideaId = ideas.id')
    .where('ideas.userId != ? AND ideas.id NOT IN (?) AND response_requests.in_progress = ?', user.id, ideasUserHasAlreadyCommentedOn, false)
    .first
  end
end
