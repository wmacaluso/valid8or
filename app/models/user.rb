class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :trackable, :validatable, :omniauthable,
    omniauth_providers: [:facebook, :google_oauth2, :linkedin]

  def provider
    @provider
  end

  def self.find_for_google_oauth2(access_token, signed_in_resource=nil)
    data = access_token.info
    user = User.where(:email => data["email"]).first
    unless user
      user = User.create(name: data["name"],
                         email: data["email"],
                         password: Devise.friendly_token[0,20]
                        )
    end
    user.save
  end

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
      #user.name = auth.info.name   # assuming the user model has a name
      #user.image = auth.info.image # assuming the user model has an image
    end
  end

  def addIdeaAmmo(ammoToAdd, user)
    user.ideaAmmo = user.ideaAmmo.nil? ? 0 : user.ideaAmmo  
    user.ideaAmmo += ammoToAdd
    user.save
  end

  def get_ideas()
    return Idea.where('userId = ?', id)
  end

  def addComplimentaryIdeaAmmoIfEmpty(user)
    ideaAmmo = user.ideaAmmo.nil? ? 0 : user.ideaAmmo  
    if ideaAmmo == 0
      addIdeaAmmo(1, user)
    end
  end
end
