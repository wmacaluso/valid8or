class IdeaResponse < ActiveRecord::Base
  belongs_to :idea
  has_many :idea_response_comment
  attr_accessor :comments
  attr_accessor :idea

  def get_comments()
    return IdeaResponseComment.where('idea_response_id = ?', id).order(created_at: :desc)
  end

  def self.getResponsesByUser(user)
    idea_responses = where('userId = ?', user.id)
    idea_responses.map{|idea_response| idea_response.comments = idea_response.get_comments()}
    idea_responses.map{|idea_response| idea_response.idea = Idea.find(idea_response.ideaId)}
    return idea_responses
  end

  def completeResponse(user)

    # Since we've successfully reponded, now remove first response request for this idea
    @response_request = ResponseRequest.where("ideaId = ?", ideaId).order(created_at: :asc).first
    @response_request.destroy

    #award this person 1 idea ammo for reviewing!
    user.addIdeaAmmo(1, user)

    #notify idea author
    IdeaResponseMailer.feedback_received(user, self).deliver
  end

  def getIdea()
    return Idea.find(ideaId)
  end
end
