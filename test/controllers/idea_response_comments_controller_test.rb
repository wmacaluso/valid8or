require 'test_helper'

class IdeaResponseCommentsControllerTest < ActionController::TestCase
  setup do
    @idea_response_comment = idea_response_comments(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:idea_response_comments)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create idea_response_comment" do
    assert_difference('IdeaResponseComment.count') do
      post :create, idea_response_comment: { description: @idea_response_comment.description, idea_response_id: @idea_response_comment.idea_response_id, user_id: @idea_response_comment.user_id }
    end

    assert_redirected_to idea_response_comment_path(assigns(:idea_response_comment))
  end

  test "should show idea_response_comment" do
    get :show, id: @idea_response_comment
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @idea_response_comment
    assert_response :success
  end

  test "should update idea_response_comment" do
    patch :update, id: @idea_response_comment, idea_response_comment: { description: @idea_response_comment.description, idea_response_id: @idea_response_comment.idea_response_id, user_id: @idea_response_comment.user_id }
    assert_redirected_to idea_response_comment_path(assigns(:idea_response_comment))
  end

  test "should destroy idea_response_comment" do
    assert_difference('IdeaResponseComment.count', -1) do
      delete :destroy, id: @idea_response_comment
    end

    assert_redirected_to idea_response_comments_path
  end
end
