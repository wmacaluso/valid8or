require 'test_helper'

class ResponseRequestsControllerTest < ActionController::TestCase
  setup do
    @response_request = response_requests(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:response_requests)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create response_request" do
    assert_difference('ResponseRequest.count') do
      post :create, response_request: { ideaId: @response_request.ideaId }
    end

    assert_redirected_to response_request_path(assigns(:response_request))
  end

  test "should show response_request" do
    get :show, id: @response_request
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @response_request
    assert_response :success
  end

  test "should update response_request" do
    patch :update, id: @response_request, response_request: { ideaId: @response_request.ideaId }
    assert_redirected_to response_request_path(assigns(:response_request))
  end

  test "should destroy response_request" do
    assert_difference('ResponseRequest.count', -1) do
      delete :destroy, id: @response_request
    end

    assert_redirected_to response_requests_path
  end
end
