require 'test_helper'

class IdeaResponsesControllerTest < ActionController::TestCase
  setup do
    @idea_response = idea_responses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:idea_responses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create idea_response" do
    assert_difference('IdeaResponse.count') do
      post :create, idea_response: { description: @idea_response.description, ideaId: @idea_response.ideaId, ideaResponseOrdinal: @idea_response.ideaResponseOrdinal, userId: @idea_response.userId }
    end

    assert_redirected_to idea_response_path(assigns(:idea_response))
  end

  test "should show idea_response" do
    get :show, id: @idea_response
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @idea_response
    assert_response :success
  end

  test "should update idea_response" do
    patch :update, id: @idea_response, idea_response: { description: @idea_response.description, ideaId: @idea_response.ideaId, ideaResponseOrdinal: @idea_response.ideaResponseOrdinal, userId: @idea_response.userId }
    assert_redirected_to idea_response_path(assigns(:idea_response))
  end

  test "should destroy idea_response" do
    assert_difference('IdeaResponse.count', -1) do
      delete :destroy, id: @idea_response
    end

    assert_redirected_to idea_responses_path
  end
end
